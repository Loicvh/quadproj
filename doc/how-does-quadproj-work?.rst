.. _how-does-quadproj-work:

How does quadproj work?
=======================


The projection is obtained by computing exhaustively all KKT point from the optimization problem defining the projection. The authors of 1_ show that for non-cylindrical central quadrics, the solutions belong to the KKT points that consist in the intersection between:

- a unique root of a nonlinear function on a specific interval;
- a set of closed-form points.

Either set can be empty but for a nonempty quadric, at least one is nonempty and contains (one of) the projections.

The full explanation is provided in 1_.

.. _1: https://loicvh.eu/aca/abstracts/OJMO_2022.html


Elementary transformations
--------------------------

The projection problem is invariant under scaling, rotation, shifting, and partitioning. Intuitively, the projection remains the same no matter where the observer is located. This explains why most of the examples here display quadrics centered in the origin (shifted), aligned with the axes (rotated), sorted by eigenvalue (partitioned), and :math:`\gamma :=  c -\mathbf{b}^t \mathbf{d} + \mathbf{d}^t A \mathbf{d}\geq 0` (for this we just flip the equality: `A=-A`, `b=-b`, `c=-c`). We say that such a quadric is `normalized`.


Scaling
*******

Scaling refers to the variable change

.. math:: \mathbf{y} = \alpha \mathbf{x},

where :math:`\alpha \in \mathbb{R}`.
Plugging it into the quadric equation, we can see that we can define the same quadric in the scaled basic with :math:`A^* = A / \alpha^2, \mathbf{b}^* = \mathbf{b} / \alpha`.

Remark that in the general case, scaling corresponds to the change of variables

.. math:: \mathbf{y} = S \mathbf{x},

for a diagonal matrix S with (nonzero) scaling factors on the diagonal.

Shifting
********

Shifting refers to the variable change

.. math:: \mathbf{y} = \mathbf{x} - \mathbf{d},

where :math:`\mathbf{d} \in \mathbb{R}^n`.
We named this variable `d` because in our case, we will always shift with respect to the quadric center that we refer to as `d`.
Plugging it into the quadric equation, we can see that we can define the same quadric in the scaled basic with :math:`\mathbf{b}^* = \mathbf{b} -2 \mathbf{d}, c^* = c -\mathbf{b}^t \mathbf{d} + \mathbf{d}^t A \mathbf{d} =: \gamma`.

**Remark**: for cylindrical quadrics, there is not a unique center. By convention we choose the center of minimal norm, that is, we set all cylindrical indices to 0.


Rotating
********
Rotating refers to the variable change

.. math:: \mathbf{y} = V \mathbf{x} ,

where :math:`V \in \mathbb{R}^{n \times n}` is a unitary matrix (:math:`V V^t = I`).
In particular, we usually choose `V` to be the eigenvectors of `A`, or equivalently, the matrix that `diagonalize` `A`.

.. math:: A = V L V^t ,

with :math:`L \in \mathbb{R}^{n \times n}` the diagonal matrix of the (sorted) eigenvalue. We can show that the `i`-th column of `V` is the associated eigenvector of the `i`-th entry of `L`.
Plugging it into the quadric equation, we can define the same quadric in the scaled basic with :math:`\mathbf{A}^* = L, \mathbf{b}^* = V^t \mathbf{b}`. In the code, we refer to this new :math:`\mathbf{b}^*` as :code:`b_reduced`.


Partitioning
************
Partitioning refers to the variable change

.. math:: \mathbf{y} = P \mathbf{x} ,

where :math:`P \in \mathbb{R}^{n \times n}` is matrix where each line and each column have only one non-zero entry equals to 1.

By identification, the same quadric is defined with :math:`\mathbf{A}^* = P^T A P, \mathbf{b}^* = P^t \mathbf{b}`.

Pivoting
********
Pivoting refer to the operation that eliminates all entries of a column except the pivot element itself.

To pivot the vector :math:`b \in \mathbb{R}^{n}` on index :math:`p`, we define the change of variable

.. math:: \mathbf{y} = T \mathbf{x} ,

where :math:`T \in \mathbb{R}^{n \times n}` is nonsingular matrix with 1 in the diagonal and each entry :math:`T_{ip} = -b_i / b_p` for :math:`i \neq p`. We also ensure the resulting pivot element is `-1` with :math:`T_{pp} = - 1 / b_p`.

Putting all together: transformation to normalized quadrics
***********************************************************

For a suitable partitioning, rotating, pivoting, and scaling transformation we can consider the quadric is **normal** form:

.. math:: \sum_{i=1}^n \lambda_i x_i^2 - b_n x_n  = c,

with the following cases:

- **Central** quadrics: :math:`b_n = 0, c=1`;
- **Conic** quadrics: :math:`b_n = 0, c=0`;
- **Parabolic** quadrics: :math:`b_n = -1, c=0, \lambda_n = 0`.

Any of such quadrics with additional :math:`\lambda_i = 0` is said to be **cylindrical**.

