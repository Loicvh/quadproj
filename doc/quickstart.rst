Quadproj quickstart
===================

This page gives you a quick overview of the use of `quadproj`. 

The following code snippets should be run sequentially (*e.g.*, to inherit from `import`), the whole script is available in test_README_.

.. _test_README: https://gitlab.com/loicvh/quadproj/-/blob/master/tests/test_README.py

The basics
----------

A simple N dimensional example
******************************

In the following snippet, we create an object `quadproj.quadrics.Quadric`.
A quadric is a quadratic hypersurface of the form

.. math:: \mathbf{x}^t A \mathbf{x} + \mathbf{b}^t \mathbf{x} +c = 0.

It is created by providing the matrix `A`, the vector `b` and the scalar `c`.
After definition of the latter parameters, the object is created in a single line (line 17).

We then create some point `x0` that we project onto the quadric.

.. literalinclude:: tests/basics1.py
   :language: python3
   :linenos:

For data of dimension larger than three, it is unfortunately rather difficult to plot the quadric.

Visualise the solution
**********************

Let us try a simpler 2D case.

.. literalinclude:: tests/basics2.py
   :language: python3
   :linenos:

The output is given in `output/ellipse_no_circle.png` and the projection of `x0` onto the quadric (`x_project`) is depicted as a red point.
Mathematically we write the projection :math:`\mathrm{P}_\mathcal{Q}(x^0)`.


.. image:: ../output/ellipse_no_circle.png

Remark that you can obviously change the output to the current directory (or whatever path).

>>> plt.savefig('ellipse_no_circle.png')

Looking at the previous image, one may believe that the returned projection (red point) is actually **not** the closest point to 
`x0`. This *false* impression is due to the unequal axes.
As a way to verify that the projection is indeed the closest point, we run the following snippet.

.. literalinclude:: tests/basics3.py
   :language: python3
   :linenos:

Switching the parameter `flag_circle` to `True`, we verify the optimality of `x_project`.

.. image:: ../output/ellipse_circle.png

Indeed, we observe that the circle centered in `x0` with radius :math:`\| \mathbf{x} - \mathbf{x}^0 \|` is:

* tangent at `x_project` = :math:`\| \mathrm{P}_{\mathcal{Q}} (\mathbf{x}^0) \|_2` ;
* not crossing the quadric.

Quid of multiple solutions?
***************************

The projection is not unique in general! We denote as *degenerate case* the cases where `x0` is located on one of the principal axes of the quadric.

In this degenerate case, it is possible (though not always the case) that multiple points are optima.

For constructing a degenerate case we can:

- Either construct a quadric in *normal* form, *i.e.*, with a diagonal matrix `A`, a null vector `b`, and `c=-1` and define `x0` with a least one entry equal to zero;
- Or choose any quadric and select `x0` to be on any axis.


Let us illustrate the second option. We create `x0` by applying the (inverse) normalization from some `x0` with at least one entry equal to zero.

Here, we chose to be close to the center and on the longest axis of the ellipse so as to be sure that there are multiple (two) solutions.

Note that the program returns **only one solution**.
Multiple solutions is planned in future releases.

.. literalinclude:: tests/basics4.py
   :language: python3
   :linenos:

The output figure `ellipse_degenerated.png` is given below.
It can be shown that the reflection of the `x_project` along the largest ellipse axis (visible because `show_principal_axes=True`) yields another optimal solution.


.. image:: ../output/ellipse_degenerated.png



Supported quadrics for display
------------------------------

We currently support all **nonlinear** quadrics: the matrix `A` cannot be null.

In this section, we give an exhaustive list of nonlinear quadrics that can be plotted (i.e., in 1D, 2D or 3D).

1-dimensional quadrics
**********************
One dimension case is trivial: the quadric equation is a one dimensional polynomial of degree two! Depending on whether we have one or two roots, the quadric consists of one or two points.

Points
''''''

Single point
++++++++++++

A single point is a quadratic surface (of dimension 0) that can be written—up to a scale factor—as

.. math:: (x - \alpha)^2 = 0,

for a given parameter :math:`\alpha \in \mathbb{R}`. We readily identify :math:`A=1, b=-2 \alpha, c=\alpha^2`.

.. literalinclude:: tests/example_1D.py
   :language: python3
   :linenos:

.. image:: ../output/1D.png

Double points
+++++++++++++
A double point quadric is a quadratic surface (of dimension 0) that can be written—up to a scale factor—as

.. math:: (x - \alpha_1) (x - \alpha_2) = 0,

for given parameters :math:`\alpha_1, \alpha_2 \in \mathbb{R}`. We readily identify :math:`A=1, b=- \alpha_1 - \alpha_2, c=\alpha_1 \alpha_2`.

.. literalinclude:: tests/example_2D.py
   :language: python3
   :linenos:

.. image:: ../output/2D.png

2-dimensional quadrics
**********************

Lines
'''''

Single line
+++++++++++
This is the direct generalization of the single point: this can be seen as the 2D cylindrical case of the single point.

.. literalinclude:: tests/example_single_line.py
   :language: python3
   :linenos:

.. image:: ../output/single_line.png


Double lines
++++++++++++
Idem as the simple line with respect to the double points.

.. literalinclude:: tests/example_parallel_lines.py
   :language: python3
   :linenos:

.. image:: ../output/parallel_lines.png

Secant lines
++++++++++++

This is the 2D version of the :ref:`elliptic cone`. We show here a degenerated case with two solutions.


.. literalinclude:: tests/example_secant_lines.py
   :language: python3
   :linenos:

.. image:: ../output/secant_lines.png

Ellipse
'''''''

Another example of an ellipse: the 2D version of the ellipsoid. We start from one aligned with the axes (`A` is diagonal) and rotate it via a unitary matrix `V`.


.. literalinclude:: tests/example_ellipse.py
   :language: python3
   :linenos:

.. image:: ../output/ellipse.png

Hyperbola
'''''''''

We illustrate a degenerated projection onto an hyperbola: the 2D version of a :ref:`One-sheet Hyperboloid`.

In this case, there is no root to the nonlinear function (graphically, the second axis does not intersect the hyperbola). This is not an issue because two solutions are obtained from the other set of KKT points (see :ref:`how-does-quadproj-work`).

.. literalinclude:: tests/basics5.py
   :language: python3
   :linenos:

.. image:: ../output/hyperbola_degenerated.png

Parabola
''''''''
This is the 2D version of the :ref:`paraboloid<Paraboloids>`.

.. literalinclude:: tests/example_parabola.py
   :language: python3
   :linenos:

.. image:: ../output/parabola.png




3-dimensional quadrics
**********************

Ellipsoid
'''''''''

Similarly as the 2D case, we can plot ellipsoid.

.. image:: ../output/ellipsoid.png

.. literalinclude:: tests/basics6.py
   :language: python3
   :linenos:


To ease visualisation, the function `get_turning_gif` lets you write a rotating gif such as:

.. image:: ../output/ellipsoid.gif

Hyperboloids
''''''''''''
An hyperboloid is a 3D quadratic surface with two positive eigenvalues and one negative.

One-sheet hyperboloid
+++++++++++++++++++++

You can easily create a turning gif of a modification of the plots (*e.g.,* by adding the projected point) by having a look at the core of `get_turning_gif`.

.. literalinclude:: tests/basics7.py
   :language: python3
   :linenos:

.. image:: ../output/one_sheet_hyperboloid.gif

But wait! It seems that the red point is **not** the closest isn't it?!

Fortunately, this is not a bug but rather the same scaling issue from :ref:`Visualise the solution<Visualise the solution>` and plotting the iso-curve of the distance function (`flag_circle=True`), we see that the red ellipsoid (*i.e.,* a distorted ball) is tangent to the quadric at `x_project`.

.. literalinclude:: tests/basics8.py
   :language: python3
   :linenos:

.. image:: ../output/one_sheet_hyperboloid_ball.gif

Two-sheet hyperboloid
+++++++++++++++++++++


.. literalinclude:: tests/basics9.py
   :language: python3
   :linenos:

.. image:: ../output/two_sheet_hyperboloid.gif

Paraboloids
'''''''''''

A paraboloid is a 3D quadratic surface of full rank (i.e., `A|b` is of rank 3) with two non-zero eigenvalues. If the two eigenvalues are positive the paraboloid is an elliptic one, if there is one positive and one negative it is an hyperboloid paraboloid.

Elliptic paraboloid
+++++++++++++++++++


.. literalinclude:: tests/example_elliptic_paraboloid.py
   :language: python3
   :linenos:

.. image:: ../output/elliptic_paraboloid.png

Hyperbolic paraboloid
+++++++++++++++++++++

.. literalinclude:: tests/example_hyperbolic_paraboloid.py
   :language: python3
   :linenos:

.. image:: ../output/hyperbolic_paraboloid.gif

Elliptic cone
'''''''''''''

.. literalinclude:: tests/example_elliptic_cone.py
   :language: python3
   :linenos:

.. image:: ../output/elliptic_cone.png

Planes
''''''

Simple plane
++++++++++++
This can be seen as the 3D extension of the simple point (or of the simple line).

.. literalinclude:: tests/example_single_plane.py
   :language: python3
   :linenos:

.. image:: ../output/single_plane.png


Double planes
+++++++++++++
This can be seen as the 3D extension of the double points (or of the double lines).

.. literalinclude:: tests/example_parallel_planes.py
   :language: python3
   :linenos:

.. image:: ../output/parallel_planes.png

Cylinders
'''''''''

Cylindrical quadrics are quadrics with (at least) one additional degree of freedom. :ref:`Planes` can be seen as a cylindrical version of the :ref:`lines<Lines>` and/or as a cylindrical version of the :ref:`points`. A simple recipe to get a 3D cylindrical quadric is to start from the 2D quadrics, and add a third dimension totally free (that is, the third line/column of `A` and third `b` entry are equal to zero).


Ellipsoid cylinder
++++++++++++++++++

.. literalinclude:: tests/example_ellipsoid_cylinder.py
   :language: python3
   :linenos:

.. image:: ../output/ellipsoid_cylinder.png


Hyperboloid cylinder
++++++++++++++++++++

.. literalinclude:: tests/example_hyperboloid_cylinder.py
   :language: python3
   :linenos:

.. image:: ../output/hyperboloid_cylinder.gif

Paraboloid cylinder
+++++++++++++++++++

.. literalinclude:: tests/example_paraboloid_cylinder.py
   :language: python3
   :linenos:

.. image:: ../output/paraboloid_cylinder.png

Elliptic cone cylinder
++++++++++++++++++++++

.. literalinclude:: tests/example_elliptic_cone_cylinder.py
   :language: python3
   :linenos:

.. image:: ../output/elliptic_cone_cylinder.png
