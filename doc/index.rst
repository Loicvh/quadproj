.. Quadproj documentation master file, created by
   sphinx-quickstart on Tue May  5 11:56:08 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

quadproj documentation
=============================================================

**Date**: |today| **Version**: |version|

**Useful links**:
`Pypi page <https://pypi.org/project/quadproj/>`__ |
`Source Repository <https://gitlab.com/quadproj_package/quadproj>`__ |
`Issues & Ideas <https://gitlab.com/quadproj_package/quadproj/issues>`__

**quadproj** is an open-source software for projecting onto quadrics (*i.e.*, quadratic surfaces).

.. image:: ../output/one_sheet_hyperboloid_ball.gif


.. toctree::
   :maxdepth: 2
   :caption: Table of Contents
   
   installation
   quickstart
   how-does-quadproj-work?
   modules

**How to cite quadproj?**

Library presentation is made in [1] and technical explanations are provided in [2].

- [1] (2022) L. Van Hoorebeeck, and P.-A. Absil, 
  "**Quadproj: a Python package for projecting onto quadratic hypersurfaces**". (`preprint <https://arxiv.org/abs/2211.00548>`__)
- [2] (2021) L. Van Hoorebeeck, P.-A. Absil, and A. Papavasiliou, 
  "**Projection onto quadratic hypersurfaces**", submitted. (`abstract/BibTex <https://loicvh.eu/aca/abstracts/OJMO_2022.html>`__)
  
