quadproj package
================

The package `quadproj` contains several modules to **proj**\ ect a point onto a **quad**\ ric.

Given some :math:`\mathbf{x}^0 \in \mathbb{R}^n` and quadric :math:`\mathcal{Q}`, we are interested in solving

.. math::
        \min_{\mathbf{x} \in \mathbb{R}^n} \|  \mathbf{x} - \mathbf{x}^0 \|^2_2 .

The package contains the following modules:

* `quadproj.fun module`_ that contains function objects derived from the optimization
  problem we want to solve'
* `quadproj.project module`_ that contains function yield to the projection *per se*;
* `quadproj.quadrics module`_ that contains the `quadric` class.

quadproj.fun module
-------------------

.. automodule:: quadproj.fun
    :members:
    :private-members:
    :undoc-members:
    :show-inheritance:


quadproj.project module
-----------------------

.. automodule:: quadproj.project
    :members:
    :private-members:
    :undoc-members:
    :show-inheritance:


quadproj.quadrics module
------------------------

.. automodule:: quadproj.quadrics
    :members:
    :private-members:
    :undoc-members:
    :show-inheritance:

