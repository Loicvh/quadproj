
A = np.array([[1, 0, 0], [0, 0, 0], [0, 0, 0]])
b = np.array([0, 2, 0])
c = -2
param = {'A': A, 'b': b, 'c': c}
Q = Quadric(**param)

fig, ax = Q.plot()

fig.savefig(join(output_folder, 'paraboloid_cylinder.png'))
