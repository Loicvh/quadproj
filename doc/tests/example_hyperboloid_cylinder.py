A = np.array([[1, 0, 0], [0, -2, 0], [0, 0, 0]])
b = np.array([-1, 2, 0])
c = -2.1
Q = Quadric(**{'A': A, 'b': b, 'c': c})

x0 = np.array([1, -2, 0])

x_project = project(Q, x0)
fig, ax = Q.plot(show_principal_axes=True)
plot_x0_x_project(ax, Q, x0, x_project)
if save_gif:
    get_gif(fig, ax, elev=15, gif_path=join(output_folder, 'hyperboloid_cylinder.gif'))
if show:
    plt.show()
