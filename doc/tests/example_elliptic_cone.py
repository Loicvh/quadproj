param = {}
param['A'] = np.array([[4, 0, 0], [0, -2, 0], [0, 0, 10]])
param['b'] = np.array([0, 0, 0])
param['c'] = 0
Q = Quadric(**param)
fig, ax = Q.plot()

fig.savefig(join(output_folder, 'elliptic_cone.png'))
