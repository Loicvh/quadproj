param = {}
param['A'] = np.array([[1, 0, 0], [0, 2, 0], [0, 0, 0]])
param['b'] = np.array([0, 0, 1])
param['c'] = -1.5
Q = Quadric(**param)
fig, ax = Q.plot()

fig.savefig(join(output_folder, 'elliptic_paraboloid.png'))
