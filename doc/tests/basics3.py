fig, ax = Q.plot()
plot_x0_x_project(ax, Q, x0, x_project, flag_circle=True)
fig.savefig(join(output_folder, 'ellipse_circle.png'))
if show:
    plt.show()
