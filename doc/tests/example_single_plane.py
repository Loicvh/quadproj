A = np.array([[1, 0, 0], [0, 0, 0], [0, 0, 0]])
b = np.array([-2, 0, 0])

c = 1
V = ortho_group.rvs(3)
A2 = V @ A @ V.T
b2 = V @ b
Q = Quadric(A=A2, b=b2, c=c)

x0 = np.array([1, 1.5, 0])
x_project = project(Q, x0)

fig, ax = Q.plot()
plot_x0_x_project(ax, Q, x0, x_project)

fig.savefig(join(output_folder, 'single_plane.png'))
