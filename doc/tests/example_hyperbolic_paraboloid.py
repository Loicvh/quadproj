param = {}
param['A'] = np.array([[-1, 0, 0], [0, 2, 0], [0, 0, 0]])
param['b'] = np.array([0, 0, 1])
param['c'] = -1.5
Q = Quadric(**param)

x0 = np.array([0.7, -0.42, -1.45])


x_project = project(Q, x0)


fig, ax = Q.plot()

ax.grid(False)
ax.axis('off')

plot_x0_x_project(ax, Q, x0, x_project)


if save_gif:
    get_gif(fig, ax, gif_path=join(output_folder, 'hyperbolic_paraboloid.gif'))

if show:
    plt.show()

