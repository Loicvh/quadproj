cat 'basics1.py' > '../../tests/test_README.py'
cat 'basics2.py' >> '../../tests/test_README.py'

for FILE in *.py;
do
	echo $FILE;
	if [ $FILE != 'basics1.py' ] && [ $FILE != 'basics2.py' ]
	then
		cat $FILE >> '../../tests/test_README.py'
	fi
done

