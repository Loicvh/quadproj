from scipy.stats import ortho_group

A = np.array([[2, 0], [0, 1]])
b = np.array([-1, 1])
c = -2

V = ortho_group.rvs(2)
A2 = V @ A @ V.T
b2 = V @ b
c2 = -2

Q = Quadric(A=A2, b=b2, c=c2)

fig, ax = Q.plot()

fig.savefig(join(output_folder, 'ellipse.png'))
