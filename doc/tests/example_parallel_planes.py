A = np.array([[0, 0, 0], [0, -2, 0], [0, 0, 0]])
b = np.array([0, 0, 0])

c = 1
Q = Quadric(A=A, b=b, c=c)

x0 = np.array([1, 1.5, 0])
x_project = project(Q, x0)

fig, ax = Q.plot()
plot_x0_x_project(ax, Q, x0, x_project)

fig.savefig(join(output_folder, 'parallel_planes.png'))
