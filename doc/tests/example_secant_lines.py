
A = np.array([[2, 0], [0, -1.5]])
b = np.array([0, 0])
c = 0
Q = Quadric(A=A, b=b, c=c)


x0 = np.random.rand(2)
x_project = project(Q, x0)


fig, ax = Q.plot()
plot_x0_x_project(ax, Q, x0, x_project, flag_circle=True)


fig.savefig(join(output_folder, 'secant_lines.png'))
